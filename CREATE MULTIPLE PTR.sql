﻿/*
	Author: Darren Henderson (University of Kentucky)
	Last updated: 11/29/2016
	
	This file demonstrates (1) how to create path-to-root arrays and (2)
	how to remove duplicates (assuming the source ontology was erroneous).
*/

-- Multiple PTR
select M.ANCESTOR, M.DESCENDANT, M.ANCESTOR_CD, M.DESCENDANT_CD, M.ANCESTOR_PTR_ARRAY
        , M.DESCENDANT_PTR_ARRAY
	, PTR.CROSS_PTR AS CROSS_PTR_ARRAY
	, M.ANCESTOR_DEPTH
	, ARRAY_LENGTH( PTR.CROSS_PTR ,1 ) AS DESCENDANT_DEPTH
	, ARRAY_LENGTH( PTR.CROSS_PTR ,1 ) - M.ANCESTOR_DEPTH PATH_LENGTH
from METATREE M LEFT JOIN LATERAL 
	( SELECT ANCESTOR_PTR_ARRAY||DESCENDANT_PTR_ARRAY[(ARRAY_LENGTH(DESCENDANT_PTR_ARRAY,1)-1)] CROSS_PTR
	  FROM METATREE SM 
	  WHERE SM.DESCENDANT = M.DESCENDANT	  
	  order by ARRAY_LENGTH(ANCESTOR_PTR_ARRAY||DESCENDANT_PTR_ARRAY[(ARRAY_LENGTH(DESCENDANT_PTR_ARRAY,1)-1)],1) DESC
	  LIMIT 1
	  ) ptr ON TRUE
WHERE DESCENDANT = 6118
AND (select count(distinct u) from unnest(ancestor_ptr_array||descendant_ptr_array) u ) <> descendant_depth;

-- Duplicate removal
 with cte_dupes as (
 SELECT * 
 FROM (
 SELECT META_ID, PTR_ARRAY, ROW_NUMBER() OVER (PARTITION BY PTR_ARRAY ORDER BY META_ID) RN
 FROM I2B2 
 WHERE PTR_ARRAY[2] = 'Diagnoses'
 )S WHERE S.RN > 1
 ) 
 DELETE  FROM METATREE WHERE ANCESTOR IN (SELECT META_ID FROM CTE_DUPES);

